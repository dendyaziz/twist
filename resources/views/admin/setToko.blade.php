<!-- Modal -->
<div class="modal fade" id="setting" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Fitur</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        Fitur Cucian Tambahan
                    </div>
                    <div class="col-md-3">
                        Rp.30.000
                    </div>
                    <div class="col-md-3">
                        <a href="{{route('ekstra.addnew')}}"><button type="button" style="float:right" class="btn btn-primary">Tambah</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="transDetail" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Transaksi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="codeOnly" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Mulai tambah pegawai!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-auto" style="padding-bottom: 0px;padding-top: 30px">
                    <center>
                        <div class="form-group row">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Kode Laundry Anda</label>
                                <input type="text" class="form-control form-control-lg" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Kode Laundry" value="{{session('kode_toko')}}" readonly>
                                <small id="emailHelp" class="form-text text-muted">Berikan ke calon pegawai</small>
                            </div>
                        </div>
                    </center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Oke</button>
            </div>
        </div>
    </div>
</div>

@if(session()->has('alert') && session()->get('alert')=="modal-codeOnly")
    <script type="text/javascript">
        $('#codeOnly').modal('show');
    </script>
@endif