@extends('admin.app')

@section('content-nosidebar')

    <div class="container">
        <div class="row vertical-align">

            <center>
                <h1>Anda belum terdaftar pada Laundry manapun</h1><br>
                <a href="{{route('laundry.create')}}"><button type="button" class="btn btn-primary">Buka Laundry!</button></a>
                <button type="button" class="btn btn-light" data-toggle="modal" data-target="#vacancy">Daftar Pegawai</button>
            </center>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="vacancy" tabindex="-1" role="dialog" aria-labelledby="vacancy" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Pendaftaran Pegawai</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('user.apply')}}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group col-md-10 mx-auto">
                            @if(session()->has('alert'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{session('msg')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <label for="kodeLaundry">Kode Laundry</label>
                            <input type="text" class="form-control" name="kode" id="kodeLaundry" aria-describedby="kodeHelp" placeholder="Masukkan Kode Laundry" required>
                            <small id="kodeHelp" class="form-text text-muted">Tanyakan Kode Laundry pada pemilik laundry</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" >Daftar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if(session()->has('alert'))
        <script type="text/javascript">
            $('#vacancy').modal('show');
        </script>
    @endif
@endsection

