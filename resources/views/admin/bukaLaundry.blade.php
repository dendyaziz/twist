@extends('admin.app')

@section('content-nosidebar')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Buka Laundry') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('laundry.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">Nama Laundry*</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Liquid Laundry, Laundry Asrama" required>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="notelp" class="col-md-4 col-form-label text-md-right">No. Telp</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="no_hp" id="nama" placeholder="Nomor Telepon">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="alamat" id="nama" placeholder="Alamat Laundry">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Daftarkan') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
