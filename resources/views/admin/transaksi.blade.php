@extends('admin.index')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="sub-content col-md-4">
                {{--{{dd(session())}}--}}
                <h1>Transaksi</h1>
                <hr>
                @if(session()->has('alert-trans') && session('alert-trans')=="success")
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        Laundry telah masuk dalam proses dengan <strong> No. {{session('id')}}</strong> dan biaya <strong>{{session('biaya')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('alert-trans') && session('alert-trans')=="error")
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        laundry gagal diinput, mohon coba lagi
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                <form action="{{route('transaksi.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="weight">Berat Cucian*</label>
                        <div class="input-group">
                            <input type="number" step=".1" max="100" min=".1" class="form-control input-weight" name="berat" id="weight" placeholder="0" aria-label="Recipient's username" aria-describedby="basic-addon2" required autofocus>
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">KG</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pelanggan">Durasi Laundry*</label>
                        <div class="input-group mb-3 col-md-6 input-group-lg" style="padding:0!important;">
                            <select class="custom-select custom-select-lg" name="lama" id="inputGroupSelect02" required>
                                {{--<option selected disabled>Choose...</option>--}}
                                <option value=1>Satu</option>
                                <option value=2 selected>Dua</option>
                                <option value=3>Tiga</option>
                                <option value=4>Empat</option>
                            </select>
                            <div class="input-group-append">
                                <label class="input-group-text" for="inputGroupSelect02">Hari</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pelanggan">Nama Pelanggan</label>
                        <input type="text" style="width:100%!important;margin-bottom:5px;" class="form-control" name="customer" id="customer" placeholder="Optional">
                            {{--<div class="form-check col-md-4">--}}
                                {{--<div class="custom-control custom-checkbox">--}}
                                    {{--<input type="checkbox" class="custom-control-input" id="customControlAutosizing">--}}
                                    {{--<label class="custom-control-label" for="customControlAutosizing">Non-member</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                    </div>
                    <div class="form-group">
                        <label for="pelanggan">Alamat Email Pelanggan</label>
                        <input type="email" style="width:100%!important;margin-bottom:5px;" class="form-control" name="email" id="email" placeholder="Optional">
                        <small id="emailHelp" class="form-text text-muted">Isi untuk mendapat pemberitahuan Laundry selesai</small>
                    </div>
                    <div class="form-group">
                        <label for="pelanggan">Tambahan/Ekstra</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="desk" onfocus="document.getElementById('ekstra10k').setAttribute('selected','selected')" placeholder="Selimut, Sprei, ..">
                            <select class="custom-select col-md-4" name="biaya" id="inputGroupSelect02">
                                {{--<option selected disabled>Choose...</option>--}}
                                <option selected disabled>Harga</option>
                                <option value=5000>Rp.5.000</option>
                                <option id="ekstra10k" value=10000>Rp.10.000</option>
                                <option value=15000>Rp.15.000</option>
                                <option value=20000>Rp.20.000</option>
                            </select>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
            <div class="col-md-1"></div>
            <div class="sub-content col-md-4" style="background-color:rgb(123,123,123,0)">
                <h1>On-process</h1>
                <hr>
                @foreach($data as $item)

                    <div class="on-process-panel row" id="process{{$item->id_trans}}" data-toggle="modal-" data-target="#exampleModal">
                        <div class="col-3" style="padding:0px">
                            No: <span class="value id">{{$item->id_trans}}</span>
                        </div>
                        <div class="col-4" style="padding:0px">
                            DD: <span class="value">{{DateTime::createFromFormat("U", strtotime($item->duedate))->format('d/m')}}</span>
                        </div>
                        <div class="progress-button">
                            @if(session()->has('ekstra'))
                                <a href="#" class="fas fa-plus info"><span class="caption">Ekstra</span></a>
                            @endif
                                <a href="{{route('transaksi.done', ['id'=>$item->id_trans])}}" class="fas fa-check done collapsed" onclick="document.getElementById('process{{$item->id_trans}}').classList.toggle('process-removed');"><span class="caption">Selesai</span></a>
                        </div>
                    </div>

                @endforeach
                @if($any)
                    <center><a href="{{route('transaksi.index')}}" class="link">Lihat Semua</a></center>
                @else
                    <a class="link">Tidak ada laundry dalam antrian</a>
                @endif
            </div>
        </div>
    </div>


@endsection




