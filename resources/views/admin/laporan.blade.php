@extends('admin.index')

@section('content')

    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <h2 class="card-title">Daftar Transaksi</h2>
                    </div>
                    <hr>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <th>ID/No</th>
                        <th>Customer</th>
                        <th>Berat Laundry</th>
                        <th>Tanggal Masuk</th>
                        <th>Tanggal Pengambilan</th>
                        <th>Harga Total</th>
                        <th>Status</th>
                        <th>#</th>
                        </thead>
                        <tbody>
                                            {{--{{dd($data)}}--}}
                        @foreach($data as $item)
                            <tr style="cursor: pointer" > {{-- data-toggle="modal" data-target="#transDetail"--}}
                                <td>{{$item->id_trans}}</td>
                                <td>{{is_null($item->customer)? "-" : $item->customer}}</td>
                                <td>{{$item->berat.' KG'}}</td>
{{--                                {{(dd($item->created_at)->format('d m Y'))}}--}}
                                <td>{{DateTime::createFromFormat("U", strtotime($item->tglmasuk))->format('d M Y')}}</td>
                                <td>{{DateTime::createFromFormat("U", strtotime($item->duedate))->format('d M Y')}}</td>
{{--                                <td>{{is_null($item->role)?'-':$item->role}}</td>--}}
                                <td>{{'Rp.'.number_format($item->hargatotal , 0, ',', '.')}}</td>
                                <td>{{$item->done ? 'Selesai' : 'On-Process'}}</td>
                                <td>@if(!$item->done)
                                        @if(session()->has('ekstra'))
                                            <button type="button" class="btn btn-light btn-sml">Ekstra</button>
                                        @endif
                                        <a  href="{{route('transaksi.done', ['id'=>$item->id_trans])}}" ><button type="button" class="btn btn-success">Selesai</button></a>
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{--table.table.table-striped.table-hover>(thead>(th*5))+tbody--}}
                </div>
            </div>
        </div>
    </div>

@endsection



