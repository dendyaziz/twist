<?php
    $path = \Request::route()->getName();
?>

<div class="sidebar">
    {{--{{dd(session('role'))}}--}}
    <a class="item bars" onclick="document.getElementsByClassName('sidebar')[0].classList.toggle('collapsed');document.getElementsByClassName('menu-btn')[0].classList.toggle('fa-chevron-left');">
        <i class="fas fa-bars menu-btn"></i>
    </a>
    <a class="item {{$path=='transaksi.create'? 'active' : ''}}" href="{{route('transaksi.create')}}">{{--active--}}
        <i class="fas fa-calculator"></i>
        Kasir
    </a>
    @if(session('role') == "admin")
        <a class="item {{$path=='transaksi.index'? 'active' : ''}}" href="{{route('transaksi.index')}}">
            <i class="fas fa-sticky-note"></i>
            Laporan
        </a>
        <a class="item {{$path=='user.index'? 'active' : ''}}" href="{{route('user.index')}}">
            <i class="fas fa-user"></i>
            User
        </a>
        <a class="item" href="#" data-toggle="modal" data-target="#setting">
            <i class="fas fa-plus"></i>
            Tambah Fitur
        </a>
    @endif
</div>