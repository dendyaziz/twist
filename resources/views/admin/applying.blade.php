@extends('admin.app')

@section('content-nosidebar')

    <div class="container">
        <div class="row vertical-align">

            <center>
                <h1 style="margin-bottom:0px"><span style="font-weight: 100;">Anda sedang mendaftar pada </span><br>{{session('nama_toko')}}</h1><br>
                <p>Tunggu pemilik laundry menerima lamaran anda</p>
                <a href="{{route('user.out')}}"><button type="button" class="btn btn-danger">Batalkan Lamaran</button></a>
                {{--<button type="button" class="btn btn-light" data-toggle="modal" data-target="#vacancy">Batalkan Lamaran</button>--}}
            </center>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="vacancy" tabindex="-1" role="dialog" aria-labelledby="vacancy" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Pendaftaran Pegawai</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('user.apply')}}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group col-md-10 mx-auto">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Laundry tidak ditemukan!</strong> Periksa kode Laundry kembali.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <label for="kodeLaundry">Kode Laundry</label>
                            <input type="text" class="form-control" name="kode" id="kodeLaundry" aria-describedby="kodeHelp" placeholder="Masukkan Kode Laundry" required>
                            <small id="kodeHelp" class="form-text text-muted">Tanyakan Kode Laundry pada pemilik laundry</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" >Daftar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection