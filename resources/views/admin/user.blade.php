@extends('admin.index')

@section('content')

    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <h2 class="card-title">Daftar Pegawai</h2>
                    </div>
                    <hr>
                    <div>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#codeOnly">Kode Laundry</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <th>Nama User</th>
                        <th>Role</th>
                        <th>E-mail</th>
                        <th>Terdaftar</th>
                        <th>#</th>
                        </thead>
                        <tbody>
                                            {{--{{dd($data)}}--}}
                        @foreach($data as $item)
                            <tr>
                                <td>{{$item->name}}</td>
                                <td>{{is_null($item->role)?'-':$item->role}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->created_at}}</td>
                                <td>
                                    @if(is_null($item->role))
                                        <a href="{{route('user.hire', ['$id_pegawai'=>$item->id_pegawai, 'role'=>2])}}">
                                            <button class="btn btn-primary">Terima</button>
                                        </a>
                                    @else
                                        <form action="{{route('user.destroy', ['id'=>$item->id_pegawai])}}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-light">Keluarkan</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{--table.table.table-striped.table-hover>(thead>(th*5))+tbody--}}
                </div>
            </div>
        </div>
    </div>

@endsection



