<!DOCTYPE html>
<html>
<body>
<p>@if(!is_null($data->toko->alamat))Hai {{explode(' ',trim( $data->customer ))[0]}}, @endif Laundry kamu dengan No. {{$data->id}} telah selesai dilaundry dengan total harga Rp.{{number_format($data->hargatotal , 0, ',', '.')}}.</p>
<p>
    Silahkan ambil pada <strong>{{$data->toko->nama}}</strong>
    @if(!is_null($data->toko->alamat)) dengan alamat {{$data->toko->alamat}}@endif
    @if(!is_null($data->toko->no_hp)) atau hubungi {{$data->toko->no_hp}}@endif
</p>
<p>Terima kasih<br>Admin {{$data->toko->nama}}</p>
{{--{{dd($data)}}--}}

</body>
</html>