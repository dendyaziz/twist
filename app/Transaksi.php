<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = "transaksi";
    protected $fillable = [
        'id_toko',
        'id_pegawai',
        'hargasekilo',
        'berat',
        'customer',
        'email',
        'duedate',
        'tglmasuk',
        'hargatotal',
    ];

    public function toko()
    {
        return $this->belongsTo(Toko::class,'id_toko','id');
    }
}
