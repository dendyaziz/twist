<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ekstra extends Model
{
    protected $table = "ekstra";
    protected $fillable = [
        'biaya',
        'desk',
        'id_trans',
    ];
}
