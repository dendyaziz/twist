<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
    protected $table = "toko";
    protected $fillable = [
        'nama',
        'kode',
        'id_pemilik',
        'no_hp',
        'alamat',
    ];

    public function transaksi()
    {
        return $this->hasMany(Transaksi::class,'id_toko','id');
    }

}
