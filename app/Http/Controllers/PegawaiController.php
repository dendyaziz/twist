<?php

namespace App\Http\Controllers;


use App\Pegawai;
use App\Toko;
use Illuminate\Http\Request;
use Session;
use Auth;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pegawai::select('*','pegawai.id as id_pegawai')->join('users', 'users.id', '=', 'pegawai.id_user')->where('id_user','!=',Auth::user()->id)->where('pegawai.id_toko', Session::get('id_toko'))->get();
        return view('admin.user')->with(['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function out()
    {
//        dd(Auth::user()->id);
        $data = Pegawai::where('id_user', Auth::user()->id)->firstOrFail();
        $data->delete();
        return back()->with(['alert'=>'success', 'msg'=>'Berhasil']);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);
        $item = new Pegawai($data);
        if($item->save())
            return true;
        return false;
    }

    public function storeOwner($id_toko,$id_user)
    {
        $request = new \Illuminate\Http\Request();
        $request->setMethod('POST');

        $request->request->add(['id_toko' => $id_toko]);
        $request->request->add(['id_user' => $id_user]);
        $request->request->add(['role' => 1]);
        $this->store($request);
//        if($this->store($request))
//            return $this->showNewToko($id_user);
//        return $this->show($id_user);
    }

    public function apply(Request $request)
    {
        $data = $request->except(['_token']);
        $toko = Toko::where('kode',$data['kode'])->get();
//        dd($toko[0]->id);
//        $data['kode']

       if(count($toko)>0){
           $request = new \Illuminate\Http\Request();
           $request->setMethod('POST');

           $request->request->add(['id_toko' => $toko[0]->id]);
           $request->request->add(['id_user' => Auth::user()->id]);
//        $request->request->add(['role' => 1]);
           $this->store($request);
           return back();
       }else{
           return back()->with(['alert'=>'error', 'msg'=>"Laundry tidak ditemukan!"]);
       }

//        $item = new Pegawai($data);
//        if($item->save())
//            return true;
//        return false;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

//        return view('admin.index', compact(['data']));
    }

    public function hasLaundry(){
        $data = Pegawai::where('id_user', Session::get('id_user_login'))->get();
//        dd($data[0]->id_toko);
//        Session::set('variableName', $data->id_toko);
        if(count($data) > 0){
            Session::put('id_toko', $data[0]->id_toko);
            Session::put('hasLaundry', true);
            return true;
        }
        Session::put('hasLaundry', false);
        return false;
    }

    public function showNewToko($id)
    {
        $data = Pegawai::join('toko', 'toko.id', '=', 'pegawai.id_toko')->where('id_user',$id)->get();
//        dd($data);
        return view('admin.index', compact(['data']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    public function hire($id_pegawai, $role)
    {
        $data = Pegawai::where('id', $id_pegawai)->update(['role' => $role]);
        return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pegawai::where('id', $id)->delete();
        return back();
    }
}
