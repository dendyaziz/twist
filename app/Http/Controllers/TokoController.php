<?php

namespace App\Http\Controllers;

use App\Toko;
use Illuminate\Http\Request;
use Auth;

class TokoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.unemp');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bukaLaundry');
    }



    public function applying()
    {
        return view('admin.applying');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kode = array_merge(range('a', 'z'), range('A', 'Z'), range (0,9));

        shuffle($kode);
        $kode = substr(implode($kode), 0, 6);
        $request->request->add(['kode' => $kode]);
        $request->request->add(['id_pemilik' => Auth::user()->id]);
        $data = $request->except(['_token']);
//        dd($data);
        $item = new Toko($data);
        if($item->save()){
            app('App\Http\Controllers\PegawaiController')->storeOwner($item->id,$item->id_pemilik);
            return redirect(route('transaksi.create'))->with(['alert'=>'modal-codeOnly', 'msg'=>$kode]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
