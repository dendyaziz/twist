<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Pegawai;
use App\Transaksi;
use Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Input;
use App\Jobs\SendEmail;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Transaksi::select('*','transaksi.id as id_trans')->leftJoin('ekstra', 'transaksi.id', '=', 'ekstra.id_trans')->where('id_toko',session('id_toko'))->orderBy('duedate', 'asc')->get(); //->where('id_user','!=',Auth::user()->id)->where('pegawai.id_toko', Session::get('id_toko'))
        return view('admin.laporan')->with(['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Transaksi::select('*','transaksi.id as id_trans')->leftJoin('ekstra', 'transaksi.id', '=', 'ekstra.id_trans')->where('id_toko',session('id_toko'))->where('done',false)->orderBy('duedate', 'asc')->take(5)->get(); //->where('id_user','!=',Auth::user()->id)->where('pegawai.id_toko', Session::get('id_toko'))
        return view('admin.transaksi')->with(['data'=>$data, 'any'=>count($data) > 0]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set("Asia/Jakarta");
//        Asia/Jakarta
//                dd($request);
        $request->request->add(['duedate' => substr(now()->addDays($request->get('lama')), 0 , 19)]);
        $request->request->add(['tglmasuk' => now()]);
        $request->request->add(['id_toko' => session('id_toko')]);
        $request->request->add(['hargasekilo' => 5000]);
        $request->request->add(['id_pegawai' => session('id_pegawai')]);
        $biaya = (($request->get('hargasekilo')*$request->get('berat'))+(!is_null($request->get('desk')) ? $request->get('biaya') : 0))*2/$request->get('lama');
        $request->request->add(['hargatotal' => $biaya]);
//        dd($request->get('durasi'));
        $data = $request->only(['berat','customer','email','duedate','tglmasuk','id_toko','id_pegawai','tglmasuk','hargasekilo','hargatotal']);
        $item = new Transaksi($data);

        if($item->save()){
            $request->request->add(['id_trans' => $item->id]);
//            dd($request);
            if(!is_null($request->get('desk')))
                app('App\Http\Controllers\EkstraController')->store($request);

            return back()->with(['alert-trans'=>'success', 'id'=>$item->id, 'biaya'=>"Rp.".number_format($biaya , 0, ',', '.')]);
        }else{
            return back()->with(['alert-trans'=>'error']);
        }
//        }else{trans-success
//            return back()->with(['alert'=>'error', 'msg'=>"Laundry tidak ditemukan!"]);
//        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function done($id_trans)
    {
        date_default_timezone_set("Asia/Jakarta");
        $data = Transaksi::with('toko')->where('id', $id_trans)->get();
        if(!is_null($data[0]->email)){
            dispatch(new SendEmail($data[0]));
        }

        Transaksi::where('id', $id_trans)->update(['done' => true]);
        return back();
    }
}