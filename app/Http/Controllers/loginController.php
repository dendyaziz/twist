<?php

namespace App\Http\Controllers;
//namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Auth;
use Session;

class loginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('id_user_login', Auth::user()->id);
        return redirect()->route('transaksi.create');

        $hasLaundry = app('App\Http\Controllers\PegawaiController')->hasLaundry();

        if($hasLaundry)
            return view('admin.index');
        else
            return view('admin.unemp');
//        return;
    }
}
