<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Pegawai;
use Session;

class HasntLaundry
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = Pegawai::join('toko', 'toko.id', '=', 'pegawai.id_toko')->where('id_user', Auth::user()->id)->get();
        if(count($data) == 0){
            return $next($request);
        }else{
            Session::put('nama_toko', $data[0]->nama);
            $data = Pegawai::join('toko', 'toko.id', '=', 'pegawai.id_toko')->where('id_user', Auth::user()->id)->whereNotNull('role')->get();
            if(count($data) > 0){
                Session::put('id_toko', $data[0]->id_toko);
                Session::put('kode_toko', $data[0]->kode);
                Session::put('role', $data[0]->role);
                Session::put('id_pegawai', $data[0]->id);
                return redirect()->route('transaksi.create');
            }else{
                return redirect()->route('laundry.applying');
            }
        }
    }
}