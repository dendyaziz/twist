<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Pegawai;
use Session;


class NotEmployee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd(session());
        $data = Pegawai::join('toko', 'toko.id', '=', 'pegawai.id_toko')->where('id_user', Auth::user()->id)->get();
//        dd("a");
//        dd($request,route('laundry.'));
//        Session::set('variableName', $data->id_toko);
        if(count($data) > 0){
            Session::put('nama_toko', $data[0]->nama);
            $data = Pegawai::join('toko', 'toko.id', '=', 'pegawai.id_toko')->where('id_user', Auth::user()->id)->whereNotNull('role')->get();
            if(count($data) > 0){
                Session::put('id_toko', $data[0]->id_toko);
                Session::put('kode_toko', $data[0]->kode);
                Session::put('role', $data[0]->role);
                Session::put('id_pegawai', $data[0]->id);
                return redirect()->route('transaksi.create');
            }else{
                return $next($request);
            }

//            dd("a");
//            return true;
        }
//        dd("a");
//        return redirect()->route('laundry.create');
//        return $next($request);
//        if(session()->has('id_toko'))
//            $request->session()->flush();
        return redirect()->route('laundry.index');
    }
}
