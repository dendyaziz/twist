<?php

Auth::routes();

Route::get('/', 'loginController@index')->name('loginPage');

Route::group(['middleware' => ['auth']], function(){
    Route::group(['middleware' => ['notEmployee']], function() {
        Route::get('/laundry/applying', 'TokoController@applying')->name('laundry.applying');
        Route::get('/user/out', 'PegawaiController@out')->name('user.out');
    });
    Route::group(['middleware' => ['hasntLaundry']], function() {
        Route::get('/laundry', 'TokoController@index')->name('laundry.index');
        Route::get('/laundry/create', 'TokoController@create')->name('laundry.create');

        Route::post('/laundry/store', 'TokoController@store')->name('laundry.store');
        Route::post('/user/apply', 'PegawaiController@apply')->name('user.apply');
    });
    Route::group(['middleware' => ['hasLaundry']], function() {
        Route::get('/user/{id_pegawai}/{role}', 'PegawaiController@hire')->name('user.hire');
        Route::get('/transaksi/done/{id_trans}', 'TransaksiController@done')->name('transaksi.done');
        Route::get('/ekstra/addekstra', 'EkstraController@addekstra')->name('ekstra.addnew');

        Route::resource('/laundry', 'TokoController')->except(['index','create','store']);
        Route::resource('/user', 'PegawaiController');
        Route::resource('/transaksi', 'TransaksiController');
        Route::resource('/transaksi/ekstra', 'EkstraController');
//        Route::post('/mail/send', 'MailController@send')->name('send');
        Route::get('/mail/send/{data}', 'MailController@send')->name('send');

    });
});

?>