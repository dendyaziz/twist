<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransaksiCreated extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->increments('id');
            $table->double('berat', 8,3);
            $table->text('customer')->nullable();
            $table->text('email')->nullable();
            $table->integer('id_toko')->unsigned();
            $table->integer('id_pegawai')->unsigned();
            $table->integer('hargasekilo');
            $table->dateTime('duedate');
            $table->dateTime('tglmasuk');
            $table->integer('hargatotal');
            $table->boolean('done')->default(false);
            $table->timestamps();

            $table->foreign('id_toko')->references('id')->on('toko');
            $table->foreign('id_pegawai')->references('id')->on('pegawai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
